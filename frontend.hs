﻿module Frontend where

import System.IO.Unsafe (unsafePerformIO)
import System.IO.Error
import Data.Maybe (fromJust)
import System.IO
import Backend

-------------- selectMenu start --------------
notCorrectChar x db fun = do
	putStrLn "\nWpisano niepoprawny znak. "
	putStr "Proszę wpisać cyfrę z zakresu "
	putStr x
	putStr ": "
	y <- getLine
	fun y db
-------------- selectMenu end --------------

-------------- createNewTask start --------------
getNameString = do
	putStr "Nazwa: "
	nameString <- getLine
	if (length nameString) == 0 then do	putStrLn "Nie wpisano nazwy!"
						getNameString
	else return nameString

getDateString = do
	putStr "Data: "
	dateString <- getLine
	if isDateValid (parseDate dateString) then return dateString else notCorrectDate

getTimeString = do
	putStr "Godzina rozpoczęcia: "
	timeString <- getLine
	if isTimeValid (parseTime timeString) then return timeString else notCorrectTime	

getRepeatingString = do
	putStr "Powtarzalność: "
	repeating <- getLine
	if isRepeatingValid repeating then return repeating else notCorrectRepeating

notCorrectDate = do
	putStrLn "Wpisano niepoprawną datę."
	putStrLn "Format daty to YYYY-MM-DD np. 2014-1-20"
	getDateString

notCorrectTime = do
	putStrLn "Wpisano niepoprawną godzinę."
	putStrLn "Format godziny to (0-23):(0-59) np. 9:55"
	getTimeString

notCorrectRepeating = do
	putStrLn "Wpisano niepoprawną powtarzalność."
	putStrLn "Możliwe powtarzalności to: brak, dzien, tydzien, miesiac i rok."
	getRepeatingString

isRepeatingValid repeating
	| repeating == "brak" = True
	| repeating == "dzien" = True
	| repeating == "tydzien" = True
	| repeating == "miesiac" = True
	| repeating == "rok" = True
	| otherwise = False

parseRepeating :: String -> Period
parseRepeating repeating
	| repeating == "brak" = PNone
	| repeating == "dzien" = PDay
	| repeating == "tydzien" = PWeek
	| repeating == "miesiac" = PMonth
	| repeating == "rok" = PYear

createUTCTime2 (year, month, day) (hour, min, sec) = makeUTCTime year month day hour min

createUTCTime (year, month, day) (hour, min) = makeUTCTime year month day hour min

createTask dateAndTime name repeating db = insertNewTask (dateAndTime, name, repeating) db
-------------- createNewTask end --------------

-------------- showTasks start --------------
showHeader = 	"id |    data    | godzina  | powtarz. | nazwa\n" ++
		"---|------------|----------|----------|----------"

showHeaderWithExecution =
	"id |    data    | godzina  | powtarz. | wykonane | nazwa\n" ++
	"---|------------|----------|----------|----------|----------"

getRepeating repeating 
	| repeating == PNone  = "brak    "
	| repeating == PDay   = "dzień   "
	| repeating == PWeek  = "tydzień "
	| repeating == PMonth = "miesiąc "
	| repeating == PYear  = "rok     "

showTasks [] = putStr ""
showTasks (x:xs) = do
	putStr (show (taskID x))
	if (taskID x) < 10 then putStr "  | " else putStr " | "
	putStr (show (getDay (taskUTCTime x)))
	putStr " | "
	putStr (show (getTime (taskUTCTime x)))
	putStr " | "
	putStr (getRepeating (taskRepeatAfter x))
	putStr " | "
	putStr (taskNote x)
	putStr "\n"
	showTasks xs

showTasksWithExecution [] = putStr ""
showTasksWithExecution (x:xs) = do
	putStr (show (taskID x))
	if (taskID x) < 10 then putStr "  | " else putStr " | "
	putStr (show (getDay (taskUTCTime x)))
	putStr " | "
	putStr (show (getTime (taskUTCTime x)))
	putStr " | "
	putStr (getRepeating (taskRepeatAfter x))
	putStr " |   "
	putStr (showExecution (taskExecution x))
	putStr "    | "
	putStr (taskNote x)
	putStr "\n"
	showTasksWithExecution xs
-------------- showTasks end --------------

-------------- saveFile start --------------
errorHandler e
	| isDoesNotExistError e = putStrLn "\nBłąd: Plik nie istnieje!"
	| isAlreadyInUseError e = putStrLn "\nBłąd: Plik jest używany!"
	| isPermissionError e = putStrLn "\nBłąd: Brak uprawnień do pliku!"
	| isEOFError e = putStrLn "\nBłąd: Koniec pliku!"
	| otherwise = putStrLn "\nBłąd: Nieznana przyczyna!"

createFile fileName contents = do
	handle <- openFile fileName WriteMode
	hPutStr handle contents
	hClose handle

createContent db = let
	header = "data;godzina;powtarzalność;wykonane;nazwa\n"
	tasksList = sortObjects (getObjects db)
	tasksString = createTaskString tasksList
	in header ++ tasksString

createTaskString [] = ""
createTaskString (x:xs) = createTaskLine x ++ createTaskString xs

createTaskLine x = 
	(show (getDay (taskUTCTime x))) 
	++ ";" ++ 
	(show (getTime (taskUTCTime x))) 
	++ ";" ++ 
	(showRepeating (taskRepeatAfter x))
	++ ";" ++ 
	(showExecution (taskExecution x))
	++ ";" ++ 
	(taskNote x) 
	++ "\n"

showRepeating repeating = case repeating of 
	PNone  -> "brak"
	PDay   -> "dzien"
	PWeek  -> "tydzien"
	PMonth -> "miesiac"
	PYear  -> "rok"

showExecution execution = case execution of 
	True  -> "tak"
	False -> "nie"
-------------- saveFile end --------------

-------------- openFile start --------------	
continueQuestion = do
	putStr "Czy chcesz kontynuować? (tak/nie): "
	answer <- getLine
	if (answer == "tak") then return True else
		if (answer == "nie") then return False else continueQuestion
		
tryOpenFile fileName db = do
	handle <- openFile fileName ReadMode
	db2 <- readTasks handle db "data;godzina;powtarzalność;wykonane;nazwa"
	hClose handle
	return db2
	
readTasks handle db header = if (unsafePerformIO (hIsEOF handle)) then return db else do 
	line <- hGetLine handle
	if line == header then readTasks handle db header else
		if line == "" then return db else
			readTasks handle (parseLine line db) header

parseLine line db = if isLineTextValid (parseLineText line)
	then createNewTaskWithExecution (fromJust (parseLineText line)) db
	else db

createNewTaskWithExecution (dateString, timeString, repeatingString, executedString, name) db = let
	maybeDate = parseDate dateString
	dateInt = fromJust maybeDate
	maybeTime = parseTimeWithSec timeString
	timeInt = fromJust maybeTime
	dateAndTime = createUTCTime2 dateInt timeInt
	repeating = parseRepeating repeatingString
	executed = parseExecution executedString
	in insert (dateAndTime, repeating, executed, name) db

parseExecution execution = case execution of
	"tak" -> True
	"nie" -> False
-------------- openFile end --------------

-------------- deleteTask start --------------
showIdQuestion = do
	putStr "\nPodaj id zadania lub wpisz 0 aby wrócić do głównego menu: "
	numberString <- getLine
	if isNumberValid (parseNumber numberString)
	then return (fromJust (parseNumber numberString))
	else notCorrectNumber

notCorrectNumber = do
	putStrLn "Wpisana wartość nie jest liczbą."
	showIdQuestion

checkIsTaskExists id db =
	if (select id db) == [] then False else True

checkIsTaskExistsWithExecution id execution db =
	if (selectByIDAndExecution id execution db) == [] then False else True
-------------- deleteTask end --------------

createDay (year, month, day) = getDay time where
	time = makeUTCTime year month day 0 0

quit = putStrLn "Do widzenia :)"
