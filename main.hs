﻿-- Program Przypomnienia na przedmiot SPOP

import Data.Maybe (fromJust)
import System.IO.Unsafe (unsafePerformIO)
import System.IO.Error
import Backend
import Frontend

main = mainMenu (emptyWithDay getCurrentDay)

-------------- menu start --------------
mainMenu db = do
	putStrLn "\nMenu główne:"
	putStrLn "1. Utwórz nowe zadanie"
	putStrLn "2. Pokaż wszystkie zadania"
	putStrLn "3. Pokaż zaplanowane zadania"
	putStrLn "4. Pokaż zrealizowane zadania"
	putStrLn "5. Pokaż zadania zaplanowane na dzisiaj"
	putStrLn "6. Pokaż zadania zrealizowane dzisiaj"
	putStrLn "7. Oznacz wybrane zadanie jako zrealizowane"
	putStrLn "8. Usuń wybrane zadanie"
	putStrLn "9. Usuń wszystkie zadania"
	putStrLn "10. Usuń zaplanowane zadania"
	putStrLn "11. Usuń zrealizowane zadania"
	putStrLn "12. Wczytaj terminarz z pliku"
	putStrLn "13. Zapisz terminarz do pliku"
	putStrLn "14. Ustaw dzisiejszą datę"
	putStrLn "15. Wyjście"
	putStr "Wybierz z menu pozycję nr: "
	x <- getLine
	selectMainMenu x db

selectMainMenu x db = case x of 
	"1"  -> createNewTask db
	"2"  -> showAllTasks db
	"3"  -> showAllPlannedTasks db
	"4"  -> showAllRealizedTasks db
	"5"  -> showTodayPlannedTasks db
	"6"  -> showTodayRealizedTasks db
	"7"  -> realizePlannedTask db
	"8"  -> deleteTask db
	"9"  -> deleteAllTasks db
	"10" -> deleteAllTasksByExecution False db
	"11" -> deleteAllTasksByExecution True db
	"12" -> startOpenFile db
	"13" -> saveFile db
	"14" -> setCurrentDay db
	"15" -> quit
	_    -> notCorrectChar "1-15" db selectMainMenu
-------------- menu end --------------

-------------- createNewTask start --------------
createNewTask db = do 
	putStrLn "\nTworzenie nowego zadania."
	name <- getNameString
	date <- getDateString
	time <- getTimeString
	repeating <- getRepeatingString
	putStrLn "\nNowe zaplanowane zadanie zostało utworzone."
	endCreateNewTask name date time repeating db

endCreateNewTask name dateString timeString repeatingString db = let
	maybeDate = parseDate dateString
	dateInt = fromJust maybeDate
	maybeTime = parseTime timeString
	timeInt = fromJust maybeTime
	dateAndTime = createUTCTime dateInt timeInt
	repeating = parseRepeating repeatingString
	db2 = createTask dateAndTime name repeating db 
	in mainMenu db2
-------------- createNewTask end --------------

-------------- showTasks start --------------
showAllTasks db = do
	putStrLn "\nLista wszystkich zadań:"
	putStrLn showHeaderWithExecution
	showTasksWithExecution (sortObjectsByDate (getObjects db))
	mainMenu db

showAllPlannedTasks db = do 
	putStrLn "\nLista wszystkich zaplanowanych zadań:"
	putStrLn showHeader
	showTasks (sortObjectsByDate (selectByExecution False db))
	mainMenu db

showAllRealizedTasks db = do 
	putStrLn "\nLista wszystkich zrealizowanych zadań:"
	putStrLn showHeader
	showTasks (sortObjectsByDate (selectByExecution True db))
	mainMenu db

showTodayPlannedTasks db = do 
	putStrLn "\nLista zadań zaplanowanych na dzisiaj:"
	putStrLn showHeader
	showTasks (sortObjectsByDate (selectByDayAndExecution (getCurrDay db) False db))
	mainMenu db

showTodayRealizedTasks db = do 
	putStrLn "\nLista zadań zrealizowanych dzisiaj:"
	putStrLn showHeader
	showTasks (sortObjectsByDate (selectByDayAndExecution (getCurrDay db) True db))
	mainMenu db
------------ showTasks end --------------

------------ files start --------------
saveFile db = do
	putStr "\nPodaj nazwę pliku: "
	fileName <- getLine
	catchIOError 	(do	createFile fileName (createContent db)
				putStr "\nZapisano poprawnie zadania do pliku "
				putStrLn fileName
			) errorHandler
	mainMenu db

startOpenFile db = do
	putStrLn "\nWczytanie zadań z pliku spowoduje dopisanie nowych zadań do istniejącego kalendarza!"
	if (unsafePerformIO continueQuestion) then openFile db
	else mainMenu db

openFile db = do
	putStr "\nPodaj nazwę pliku: "
	fileName <- getLine
	result <- tryIOError 	(do	db2 <- tryOpenFile fileName db
					putStr "\nWczytano poprawnie zadania z pliku "
					putStrLn fileName
					return db2)
	case result of
		Left e -> do 	errorHandler e
				mainMenu db
		Right db2 -> mainMenu db2
------------ files end --------------

------------ deleteTask start --------------
deleteTask db = do
	putStrLn "\nLista wszystkich zadań:"
	putStrLn showHeaderWithExecution
	showTasksWithExecution (sortObjectsByDate (getObjects db))
	number <- showIdQuestion
	if number == 0 then mainMenu db else
		if (checkIsTaskExists number db)
		then do putStrLn "\nZadanie zostało usunięte."
			mainMenu (delete number db)
		else do putStrLn "\nNie istnieje zadanie o podanym id."
			deleteTask db

deleteAllTasks db = do
	putStrLn "\nWszystkie zadania zostaną usunięte!"
	if (unsafePerformIO continueQuestion)
	then do	putStrLn "\nZadania zostały usunięte."
		mainMenu (emptyWithDay (getCurrDay db))
	else mainMenu db

deleteAllTasksByExecution executed db = do
	putStr "\nWszystkie zadania "
	if executed then putStr "zrealizowane" else putStr "zaplanowane"
	putStrLn " zostaną usunięte!"
	if (unsafePerformIO continueQuestion)
	then do	putStrLn "\nZadania zostały usunięte."
		mainMenu (deleteByExecution executed db)
	else mainMenu db
------------ deleteTask end --------------

------------ realizePlannedTask start --------------
realizePlannedTask db = do
	putStrLn "\nLista wszystkich zaplanowanych zadań:"
	putStrLn showHeader
	showTasks (sortObjectsByDate (selectByExecution False db))
	number <- showIdQuestion
	if number == 0 then mainMenu db else
		if (checkIsTaskExistsWithExecution number False db)
		then do putStrLn "\nZadanie zostało oznaczone jako zrealizowane."
			mainMenu (executeTask number db)
		else do putStrLn "\nNie istnieje zaplanowane zadanie o podanym id."
			realizePlannedTask db
------------ realizePlannedTask end --------------

setCurrentDay db = do
	putStrLn "\nPodaj dzisiejszą datę."
	dateString <- getDateString
	mainMenu (setCurrDay (makeDay (fromJust (parseDate dateString))) db)