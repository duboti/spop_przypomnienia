﻿module Backend where

import System.IO.Unsafe
import Data.List (sort, sortBy)
import Data.Maybe (isJust)
import Data.Time 
import Parsing

-- data brana z systemu
getCurrentDay :: Day
getCurrentDay = utctDay (unsafePerformIO getCurrentTime)

--------------- Typ Object Start ---------------

-- typ reprezentujący cykliczność zadania 
data Period = PNone | PDay | PWeek | PMonth | PYear deriving (Show, Read, Enum, Eq, Ord) 
type ID = Int
{-
let u = select 1 d3 
let time = taskUTCTime u
utctDay time -- dzień w Day
timeToTimeOfDay (utctDayTime time) -- TimeOfDay
uct
-}
--x = Task 5 (makeUTCTime 2013 10 23 12 05) "cos" (toEnum 1)
data Object = Task {
	taskID :: ID,  
	taskUTCTime :: UTCTime, 
	taskRepeatAfter :: Period, 
	taskExecution :: Bool,  
	taskNote :: String } deriving (Show, Eq, Ord)

objectTask :: ID -> UTCTime -> Period -> Bool -> String-> Object
objectTask id time repeat execut note = Task id time repeat execut note

getDay :: UTCTime -> Day
getDay t = utctDay t

getTime :: UTCTime -> TimeOfDay
getTime t =  timeToTimeOfDay (utctDayTime t)

-- domyślne sortowanie obiektów
sortObjects a = sort a

-- sortowanie obiektów po dacie
sortObjectsByDate :: [Object] -> [Object]
sortObjectsByDate a = sortBy orderObjectsByDate a

-- funkcja porządkująca obiekty po datach
orderObjectsByDate :: Object -> Object -> Ordering
orderObjectsByDate a b 
	| (taskUTCTime a) > (taskUTCTime b) = GT
	| (taskUTCTime a) < (taskUTCTime b) = LT
	| (taskUTCTime a) == (taskUTCTime b) = EQ

--------------- Typ Object End ---------------	

--------------- Databases Start ---------------
	
-- klasa reprezentująca bazę danych
class Databases d where
	empty 		:: d
	getLastID 	:: d -> ID
	getObjects	:: d -> [Object]
	setLastID	:: ID -> d -> d
	setObjects	:: [Object] -> d -> d


data DBR = DB ID Day [Object] deriving (Show, Eq)	

-- instancjonowanie typu DBR do klasy Databases
instance Databases DBR where
	empty				= DB 0 (ModifiedJulianDay 0) [] 
	getLastID (DB i d os)		= i
	getObjects (DB i d os)		= os
	setLastID i (DB j d os)		= DB i d os
	setObjects os (DB i d ps)	= DB i d os


emptyWithDay :: Day -> DBR
emptyWithDay d	= DB 0 d []
	
getCurrDay :: DBR -> Day
getCurrDay (DB i d os)	= d

setCurrDay :: Day -> DBR -> DBR
setCurrDay d (DB i oldd os)	= DB i d os

-- dodanie elementu do bazy
-- insertNewTask (makeUTCTime 2013 10 23 12 05, toEnum 0 :: Period, "john") d0
insertNewTask :: Databases d =>(UTCTime, String, Period ) -> d -> d
insertNewTask (time, note, repeat) db = insert (time, repeat, False, note) db
	
-- dodanie elementu do bazy
-- insert (makeUTCTime 2013 10 23 12 05, PDay, True, "john") d0
insert :: Databases d =>(UTCTime, Period, Bool, String) -> d -> d
insert (_, _, _, []) db = db
insert (time, repeat, executed, note) db = setLastID i' db' where
	db'	= setObjects os' db
	os'	= o:os
	os 	= getObjects db
	o  	= objectTask i' time repeat executed note
	i' 	= 1 + getLastID db
	
-- zwraca listę elementów z bazy o danym ID
-- select 1 d0	
select :: Databases d => ID -> d -> [Object]
select id db = filter ((== id) . taskID) (getObjects db)

-- wybieranie elementów bazy za pomocą funkcji wyboru
-- selectBy (\x -> PNone == taskRepeatAfter x) d2
selectBy :: Databases d => (Object -> Bool) -> d -> [Object]
selectBy f db = filter f (getObjects db)

-- filtruje obiekty po wartości taskExecution
filterExecution :: Bool -> (Object -> Bool)
filterExecution execute  = (==execute) . taskExecution 

-- filtruje obiekty po wartości Day w taskUTCTime
filterDay :: Day -> (Object -> Bool)
filterDay day  = (==day) . utctDay . taskUTCTime 

-- wybiera obiekty z bazy które maja podaną datę
-- selectByDay (fromGregorian 2013 10 23) d3
selectByDay :: Databases d => Day -> d -> [Object]
selectByDay day db = selectBy (filterDay day) db

-- wybiera obiekty z bazy które mają podaną wartość taskExecution
-- selectByExecution True d2
selectByExecution :: Bool -> DBR -> [Object]
selectByExecution execution db = selectBy (filterExecution execution) db

-- wybiera obiekty z bazy które mają podany dzień i wykonanie
-- selectByDayAndExecution (makeDay 2013 12 24) False d3
selectByDayAndExecution :: Day -> Bool -> DBR -> [Object]
selectByDayAndExecution day execution db = filter (filterExecution execution) (selectByDay day db)

-- wybiera obiekty z bazy które mają podany id i stan wykonania
-- selectByDayAndExecution 1 False d3
selectByIDAndExecution :: ID -> Bool -> DBR -> [Object]
selectByIDAndExecution id execution db = filter (filterExecution execution) (select id db)

-- usuwa z bazy element o podanym ID
delete :: Databases d => ID -> d -> d
delete id db = setObjects (filter ((/= id) . taskID) (getObjects db)) db

-- usuwa z bazy elementy o podany wykonaniu
deleteByExecution :: Bool -> DBR -> DBR
deleteByExecution execution db = db' where
	db' = setObjects os' db
	os' = selectByExecution (not execution) db

-- ogólny update wszystkich wartości wpisu 
update :: Databases d => ID -> (UTCTime, Period, Bool, String) -> d -> d
update id values db
	| (select id db) == [] 	= db
	| otherwise 		= db' where
		db' = setObjects os' db
		os' = o':os
		os  = (filter ((/= id) . taskID) (getObjects db))
		(time, repeat, executed, note) = values
		o'  = objectTask id time repeat executed note

-- update wartości execution wpisu
updateExecution :: ID -> Bool -> DBR -> DBR
updateExecution id value db 	
	| (select id db) == [] 	= db
	| otherwise 		= update id (time, repeat, value, note) db where
		(Task i time repeat _ note) = head (select id db)


-- generuje dane do insertNewTask'a dla odnowienia zadania
nextTask :: Object -> (UTCTime, String, Period)
nextTask (Task i t r e n) = (t', n ,r) where
	time = utctDayTime t
	d = utctDay t
	t'= UTCTime (nextDay d r) time

-- zwraca dzień przesunięty o cykl podanego okresu
nextDay :: Day -> Period -> Day
nextDay d p =	case p of
		PNone 	->   d
		PDay	->   addDays 1 d
		PWeek	->   addDays 7 d
		PMonth	->   addGregorianMonthsClip 1 d
		PYear	->   addGregorianYearsClip 1 d

-- zmienia zadanie na wykonane i dodaje odnowienie o ile jest cykliczne
-- TODO: dodać generowanie odnowionego zadania w terminie pierwszym po currentDay?
executeTask :: ID -> DBR -> DBR
executeTask _ (DB 0 d []) = (DB 0 d [])
executeTask id db 	
	| (select id db) == [] = db
	| otherwise 		= db' where
		o	= head (select id db)
		nt	= nextTask o
		db'	= if (PNone==(taskRepeatAfter o) || True==(taskExecution o)) 
			then updateExecution id True db
			else insertNewTask nt (updateExecution id True db)

-- konstruktor typu UTCTime	
makeUTCTime :: Integer->Int->Int->Int->Int->UTCTime
makeUTCTime r m d h mi = UTCTime (fromGregorian r m d) (timeOfDayToTime (TimeOfDay h mi 0)) 

-- konstruktor typu Day
makeDay :: (Integer, Int, Int) -> Day
makeDay (r, m, d) = fromGregorian r m d

--------------- Databases End ---------------
                    		   
		   
d0, d1 :: DBR
d0 = empty
d1 = insertNewTask (makeUTCTime 2013 10 23 12 05, "john", toEnum 0 :: Period) d0
d2 = insertNewTask (makeUTCTime 2013 12 23 10 05, "kk", toEnum 1 :: Period) d1
d3 = insertNewTask (makeUTCTime 2013 11 25 10 05, "niedziela", toEnum 2 :: Period) (insertNewTask (makeUTCTime 2013 12 24 10 05, "buu", toEnum 2 :: Period) d2)
d4 = executeTask 2 d3
d5 = executeTask 3 d4


--------------- Parsers Start ---------------

-- parser do daty w formacie "liczba-liczba-liczba"
parserDate :: Parser (Integer, Int, Int)
parserDate = do	year <- nat
		char '-'
		month <- nat
		char '-'
		day <- nat
		return (toInteger year, month, day)
		
-- parser do czasu w formacie "liczba:liczba"		
parserTimeWithSec :: Parser (Int, Int, Int)
parserTimeWithSec = do	
		hour <- nat
		char ':'
		minute <- nat
		char ':'
		sec <- nat
		return (hour, minute, sec)		
		
-- parser do czasu w formacie "liczba:liczba"		
parserTime :: Parser (Int, Int)
parserTime = do	hour <- nat
		char ':'
		minute <- nat
		return (hour, minute)		


-- prosty parser parsuje gdy element nie jest zakazany
notChar x = do sat (/= x)

-- parser zwraca ciąg wejściowy do napotkania podanego symbolu x
parserUntil :: Char -> Parser [Char]
parserUntil x = do 	xs <- many (notChar x)
			return xs
	
-- rozprasowuje linie z csv na zapisane tam zmienne
parserLine :: Parser ([Char], [Char], [Char], [Char], [Char])
parserLine = do
	date <- parserUntil ';'
	char ';'
	time <- parserUntil ';'
	char ';'
	repeating <- parserUntil ';'
	char ';'
	executed <- parserUntil ';'
	char ';'
	name <- many item
	return (date,time,repeating,executed,name)

-- podaje wynik z parsera o ile zakończył się on poprawnie i przetworzył wszystko
unparse :: [(a, [t])] -> Maybe a
unparse [] = Nothing
unparse [(a,[])] = Just a
unparse [(_,_)] = Nothing 


-- zwraca Just (Int, Int, Int) o ile na wejściu był string w formacie "liczba-liczba-liczba" w p.p. Nothing	
parseDate :: [Char] -> Maybe (Integer, Int, Int)
parseDate i = d where
	p = parse parserDate i
	d = unparse p

-- zwraca Just(Int, Int) o ile na wejściu był string w formacie "liczba:liczba" w p.p. Nothing
parseTimeWithSec :: [Char] -> Maybe (Int, Int, Int)	
parseTimeWithSec i = t where
	p = parse parserTimeWithSec i
	t = unparse p

-- zwraca Just(Int, Int) o ile na wejściu był string w formacie "liczba:liczba" w p.p. Nothing
parseTime :: [Char] -> Maybe (Int, Int)	
parseTime i = t where
	p = parse parserTime i
	t = unparse p

-- rozbija wejście na 4 wartości rozdzielonych znakiem ';'
-- let w = parseLineText "2013-12-27;15:00:00;PNone;Super oferta"
parseLineText :: [Char] -> Maybe ([Char], [Char], [Char], [Char], [Char])	
parseLineText i = l where
	p = parse parserLine i
	l = unparse p

-- zwraca Just(Int) gdy na wejściu była liczba Nothing w p.p.	
parseNumber :: [Char] -> Maybe Int
parseNumber i = n where
	p = parse nat i
	n = unparse p

-- sprawdza czy wartość jest w podanym zakresie
inRange :: Ord a => (a,a) -> a -> Bool	
inRange (min, max) a = (a>=min) && (a<=max)	
	
-- przyjmuje wynik z parseTime i sprawdza czy dane są poprawne 
isTimeValid :: Maybe(Int, Int) -> Bool
isTimeValid Nothing = False
isTimeValid (Just(h,min)) = (inRange (0,23) h) && (inRange (0,59) min)	

-- przyjmuje wynik z parseTime i sprawdza czy dane są poprawne 
isTimeWithSecValid :: Maybe(Int, Int, Int) -> Bool
isTimeWithSecValid Nothing = False
isTimeWithSecValid (Just(h,min,sec)) = (inRange (0,23) h) && (inRange (0,59) min) && (inRange (0,59) sec)
	
-- przyjmuje wynik z parseDate i sprawdza czy dane są poprawne 
isDateValid :: Maybe(Integer, Int, Int) -> Bool
isDateValid Nothing = False
isDateValid (Just(y, m, d))	| isJust (fromGregorianValid y m d ) = True
				| otherwise = False

-- przyjmuje wynik z parseLineText i sprawdza czy wynik jest poprawny
isLineTextValid :: Maybe ([Char], [Char], [Char], [Char], [Char]) -> Bool
isLineTextValid Nothing = False
isLineTextValid (Just a) = True

-- przyjmuje wynik z parseNubmer i sprawdza czy wynik jest poprawny
isNumberValid :: Maybe Int -> Bool
isNumberValid Nothing = False
isNumberValid (Just a) = True
				
--------------- Parsers End ---------------
